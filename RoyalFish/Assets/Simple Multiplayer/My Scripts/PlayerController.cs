﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.AI;

public class PlayerController : NetworkBehaviour
{
    public GameObject bulletPrefab, FriendPrefab;
    public Transform bulletSpawn, FriendSpawn;
    public VirtualJoystick joystick;
    int forKlik = 0;
    int minusCoin;
    int minusTembak = 10;

    public LayerMask whatCanBeClick;
    private NavMeshAgent myAgent;

    void Start()
    {
        joystick = GameObject.Find("VirtualJoystick").transform.GetChild(0).GetComponent<VirtualJoystick>();
        myAgent = GetComponent<NavMeshAgent>();
    }

    void Update ()
    {
        if(!isLocalPlayer)
        {
            return;
        }

        //float x = Input.GetAxis("Horizontal") * Time.deltaTime * 150.0f;
        //float z = Input.GetAxis("Vertical") * Time.deltaTime * 3.0f;

        if(Input.GetMouseButtonDown(0))
        {
            Ray myRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hitInfo;

            if(Physics.Raycast(myRay, out hitInfo, 100, whatCanBeClick))
            {
                myAgent.SetDestination(hitInfo.point);
            }
        }

        float x = joystick.Horizontal() * Time.deltaTime * 150.0f;
        float z = joystick.Vertical() * Time.deltaTime * 3.0f;

        transform.Rotate(0, x, 0);
        transform.Translate(0, 0, z);

        if(Input.GetKeyDown(KeyCode.Space))
        {
            CmdFire();
            minusCoin = Random.Range(1, 5);
            /*if (Coin.coin >= 5)
            {
                CmdFriend();
                Coin.coin = Coin.coin - minusCoin;
            }*/
        }

        if(Input.GetKeyDown(KeyCode.M))
        {
            CmdFriend();
        }

        if(CrossPlatformInputManager.GetButtonDown("Tembak"))
        {
            forKlik = 1;
            if(forKlik == 1)
            {
                CmdFire();
            }
            forKlik++;
        }

        if(CrossPlatformInputManager.GetButtonUp("Tembak"))
        {
            forKlik = 0;
        }

        if (CrossPlatformInputManager.GetButtonDown("SpawnTeam"))
        {
            forKlik = 1;
            minusCoin = Random.Range(1, 5);
            if (Coin.coin >= 5 && forKlik == 1)
            {
                CmdFriend();
                Coin.coin = Coin.coin - minusCoin;
            }
            forKlik++;
        }

        if (CrossPlatformInputManager.GetButtonUp("SpawnTeam"))
        {
            forKlik = 0;
        }
    }

    [Command]
    void CmdFire()
    {
        GameObject bullet = (GameObject)Instantiate(bulletPrefab, bulletSpawn.position, bulletSpawn.rotation);
        bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * 6.0f;
        NetworkServer.Spawn(bullet);
        Destroy(bullet, 2);
    }

    [Command]
    void CmdFriend()
    {
        Vector3 spawnPosition = new Vector3(Random.Range(-8.0f, 8.0f), 1.0f, Random.Range(-8.0f, 8.0f));
        GameObject Friend = (GameObject)Instantiate(FriendPrefab, spawnPosition, FriendSpawn.rotation);
        Friend.GetComponent<Rigidbody>().velocity = Friend.transform.forward * 0.0f;
        NetworkServer.Spawn(Friend);
    }

    public override void OnStartLocalPlayer()
    {
        GetComponent<MeshRenderer>().material.color = Color.blue;
    }
}
