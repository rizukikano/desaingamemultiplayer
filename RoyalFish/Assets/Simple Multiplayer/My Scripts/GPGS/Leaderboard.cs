﻿
using UnityEngine;

public class Leaderboard : MonoBehaviour
{
    public void OpenLeaderboardPanel()
    {
        Social.ShowLeaderboardUI();
    }

    public void UpdateLeaderboardScore()
    {
        if (PlayerPrefs.GetInt("ScoreToUpdate", 0) == 0)
        {
            return;
        }

        Social.ReportScore(PlayerPrefs.GetInt("ScoreToUpdate", 1), GPGSIds.leaderboard_highscore, (bool success)=>
            {
            if (success)
            {
                PlayerPrefs.SetInt("ScoreToUpdate", 0);
            }

        });
    }
}
