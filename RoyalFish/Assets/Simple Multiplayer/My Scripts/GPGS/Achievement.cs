﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GooglePlayGames;

public class Achievement : MonoBehaviour
{
    public void OpenAchievementPanel()
    {
        Social.ShowAchievementsUI();
    }

   public void UnlockBronze()
    {
        Social.ReportProgress(GPGSIds.achievement_bronze, 100f, null);
    }
    public void UnlockSilver()
    {
        Social.ReportProgress(GPGSIds.achievement_silver, 100f, null);
    }
    public void UnlockGold()
    {
        Social.ReportProgress(GPGSIds.achievement_gold, 100f, null);
    }
    public void FirstLogin()
    {
        Social.ReportProgress(GPGSIds.achievement_first_login, 100f, null);
    }
}
