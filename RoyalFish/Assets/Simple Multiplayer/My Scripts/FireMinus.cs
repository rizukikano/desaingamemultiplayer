﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireMinus : MonoBehaviour {
    public int JmlMinus = 5;

    void OnCollisionEnter(Collision collision)
    {
        GameObject hit = collision.gameObject;
        Health health = hit.GetComponent<Health>();

        if (health != null)
        {
            health.TakeFire(JmlMinus);
        }

        Destroy(gameObject);
    }
}
