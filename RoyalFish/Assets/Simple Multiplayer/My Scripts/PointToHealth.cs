﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointToHealth : MonoBehaviour {

    public int JmlTambah = 5;
    public int tambahCoin = 1;

    void OnCollisionEnter(Collision collision)
    {
        GameObject hit = collision.gameObject;
        Health health = hit.GetComponent<Health>();

        if (health != null)
        {
            health.PlusHealth(JmlTambah);
        }

        Coin.coin = Coin.coin + tambahCoin;

        Destroy(gameObject);
    }
}
