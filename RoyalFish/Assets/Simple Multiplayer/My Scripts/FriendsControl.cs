﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class FriendsControl : NetworkBehaviour
{

    public GameObject bulletPrefab;
    public Transform bulletSpawn;
    float x;
    int tembak = 0;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        x = x + 0.01f;
        transform.Rotate(0, x , 0);

        tembak++;
        if(tembak == 10)
        {
            CmdFire();
            tembak = 0;
        }
    }

    [Command]
    void CmdFire()
    {
        GameObject bullet = (GameObject)Instantiate(bulletPrefab, bulletSpawn.position, bulletSpawn.rotation);
        bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * 6.0f;
        NetworkServer.Spawn(bullet);
        Destroy(bullet, 2);
    }
}
