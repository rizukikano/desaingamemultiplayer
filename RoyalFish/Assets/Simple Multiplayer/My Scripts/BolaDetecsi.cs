﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class BolaDetecsi : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.name == "Gawang1")
        {
            Destroy(col.gameObject);
            SceneManager.LoadScene("Player1Win");
        }

        if (col.gameObject.name == "Gawang2")
        {
            Destroy(col.gameObject);
            SceneManager.LoadScene("Player2Win");
        }
    }
}
